# Graphty iOS

[![GitHub license](https://img.shields.io/badge/license-MIT-lightgrey.svg?maxAge=2592000)](https://raw.githubusercontent.com/apollographql/apollo-ios/main/LICENSE) [![Swift 5 Supported](https://img.shields.io/badge/Swift-5.0-orange.svg)](https://github.com/apple/swift) 

Graphty iOS is a strong-typed, caching and persistence GraphQL client for iOS, written in Swift.

## Graphty Schema

It allows you to visualize tables and their queries and mutations for a given GraphQL server and autogenerates a custom graphty client based on a given GraphQL Schema.

Generated code for Graphty client will contain:
- Data Types
- Enums
- Network Layer
- Cache & Data Persistence
- Data Gateway

Customize your client:
- Select tables, attributes, queries, mutations and enums to include

## Graphty Client

It allows you to perform queries and mutations in a simple swifty way
